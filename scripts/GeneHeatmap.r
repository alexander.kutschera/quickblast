library(gplots)
library(RColorBrewer)

############# Settings ###########################

outputname = "CPA_enzymes_heatmap.pdf"
header = "CPA biosynthesis enzymes \nPairwise alignment of aminoacid sequences \n(PAO1 as query, e-value cutoff = 10^-9)"
color = brewer.pal(8,"YlGnBu")
data_path = "/Users/alexanderkutschera/BlastResults/LPS_OPS/"
#################################################

genedata <- read.csv(paste0(data_path, "CPA_final_results.csv"), comment.char = "#", sep = ",")
genenames <- read.csv(paste0(data_path, "CPA_final_results_genenames.csv"), comment.char = "#", sep = ",")

row.names(genedata) <- genedata$X #name columns
row.names(genenames) <- genenames$X

genedata <- genedata[-1] #and remove names from column 1
genenames <- genenames[-1]

genedata_matrix <- data.matrix(genedata)
genedata_matrix_r <- round(genedata_matrix, digits = 2) #rounds the values to 2 digits

genenames_matrix <- data.matrix(genenames)

#my_palette <- colorRampPalette(color)(n = 8) #colorpalette for heatmap n = level
# more colours: http://www.stat.columbia.edu/~tzheng/files/Rcolor.pdf

#optional to change color distribution
#col_breaks = c(seq(0, 50, length=1),
#seq(0, 55, length=1),
#seq(56, 60, length=1),
#seq(61, 65, length=1),
#seq(66, 70, length=1),
#seq(71, 75, length=1),
#seq(76, 80, length=1),
#seq(81, 85, length=1),
#seq(86, 90, length=1),
#seq(91, 95, length=1),
#seq(96, 100, length=1));

col_breaks = c(seq(0, 100, length=9));

#svg(paste0(data_path, "heatmaps/", gsub(".pdf","", outputname), ".svg"), width=14, height=7) # for .svg output, add dev.off()!
pdf(paste0(data_path, "heatmaps/", outputname), useDingbats=FALSE, width=18, height=8) # useDingbats=FALSE for pdf

heatmap.2(genedata_matrix,
          cellnote = genedata_matrix_r, #genedata_matrix_r,  # same data set for cell labels  
          main = header,                # heat map title
          cex.main=0.1,
          notecol="black",              # change font color of cell labels to black
          density.info="none",          # turns off density plot inside color legend
          trace="none",                 # turns off trace lines inside the heat map
          margins =c(8,14),           # widens margins around plot
          col=color,                    # use on color palette defined earlier
          breaks=col_breaks,            # enable color transition at specified limits
          #srtCol = 0,                  # angle of row/column labels, in degrees from horizontal
          Rowv = TRUE,
          Colv = FALSE,
          dendrogram="row")             # only draw a row dendrogram

dev.off()

#svg(paste0(data_path, "heatmaps/", gsub(".pdf","", outputname), "_wn.svg"), useDingbats=FALSE, width=25, height=8)
pdf(paste0(data_path, "heatmaps/", gsub(".pdf","", outputname), "_wn.pdf"), useDingbats=FALSE, width=35, height=10) # useDingbats=FALSE for pdf

heatmap.2(genedata_matrix,
          cellnote = genenames,         #genedata_matrix_r,  # same data set for cell labels
          main = header,                # heat map title
          cex.main=0.1,
          notecol="black",              # change font color of cell labels to black
          density.info="none",          # turns off density plot inside color legend
          trace="none",                 # turns off trace lines inside the heat map
          margins =c(8,14),           # widens margins around plot
          col=color,                    # use on color palette defined earlier
          breaks=col_breaks,            # enable color transition at specified limits
          #srtCol = 0,                  # angle of row/column labels, in degrees from horizontal
          Rowv = TRUE,
          Colv = FALSE,
          dendrogram="row")             # only draw a row dendrogram

dev.off()

svg(paste0(data_path, "heatmaps/", gsub(".pdf","", outputname), "_pure.svg"), width=17.28, height=20.262)
#pdf(paste0(data_path, "heatmaps/", gsub(".pdf","", outputname), "_wn.pdf"), useDingbats=FALSE, width=35, height=10) # useDingbats=FALSE for pdf

heatmap.2(genedata_matrix,
          #cellnote = genenames,         #genedata_matrix_r,  # same data set for cell labels
          main = header,                # heat map title
          cex.main=0.1,
          notecol="black",              # change font color of cell labels to black
          density.info="none",          # turns off density plot inside color legend
          trace="none",                 # turns off trace lines inside the heat map
          margins =c(8,14),           # widens margins around plot
          col=color,                    # use on color palette defined earlier
          breaks=col_breaks,            # enable color transition at specified limits
          #srtCol = 0,                  # angle of row/column labels, in degrees from horizontal
          Rowv = TRUE,
          Colv = FALSE,
          dendrogram="row")             # only draw a row dendrogram

dev.off()
