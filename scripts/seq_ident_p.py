#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 16 14:35:31 2018
First Version was written on Tue Aug  9 08:40:55 2016

@author: alexanderkutschera
"""

import os
import csv
from Bio import SeqIO
from Bio.Blast.Applications import NcbiblastpCommandline
from Bio.Blast import NCBIXML

# set paths WARNING they should not contain any whitespace!
data_path = "/Users/alexanderkutschera/BlastResults/LPS_OPS/" #set path for the data output
proteomes_path = data_path + "/proteome_database" #set path for the folder containing the proteome database
query_path = data_path + "/query_PAO1" #set path for the folder containing the query .fasta files
blastp_path = "/Users/alexanderkutschera/ncbi-blast-2.7.1+/bin/blastp" #usually it should just be "blastp" but it doesnt work with AWS

# gene lists I was using. They are not necessary.
raetz_genes = ["", "LpxA", "LpxC", "LpxD", "LpxH", "LpxB", "LpxK", "WaaA", "PSPTO_0182", "HtrB"] #list of biosythesis genes in the raetz pathway
core_genes =["","MsbA", "WaaL", "WapR", "putative_glycosyl_transferase", "DnpA", "mig-14", "WapH", "putative_carbamoyltransferase", "putative_kinase", "WapQ", "WaaP", "WaaG", "WaaC", "WaaF"]
osa_genes = ["", "Wzz2", "Wzz", "WbpA", "WbpB", "WbpC", "WbpD", "WbpE", "Wzy", "Wzx", "HisH2", "HisF2", "WbpG", "WbpH", "WbpI", "WbpJ", "WbpK", "WbpL", "WbpM"]
cpa_genes = ["", "WbpZ", "WbpY", "WbpX", "Wzt", "Wzm", "WbpW", "Gmd", "Rmd", "PA5455", "PA5456", "PA5457", "PA5458", "PA5459"]

all_genes =[""] #creates a list from all .fasta files in the query_path folder if you don't want to use a preset list from above
for gene in os.listdir(query_path):
    if gene.endswith(".fasta"):
        all_genes.append(gene[:-6]) #removes the .fasta, yes there are better ways to do this.
        

name = "biosynthesis"
genelist = cpa_genes # sets which gene list should be used for the blast (use all_genes if you want to blast everything in the query folder)

#use if you want to blast specific proteomes
#proteomes = ["Pci_JBC1_proteome", "Pst_DC3000_proteome"] 

#use following if you want to blast everything in the proteome folder
proteomes = list()

for filename in os.listdir(proteomes_path):
    if filename.endswith(".fasta"):
        proteomes.append(filename[:-6]) #removes the .fasta, yes there are better ways to do this. 
print "You have %i proteomes in your list" %len(proteomes)

# ---- blast ----

d = {} #init dictionary
n = {} 

#d["Raetz genes"] = raetz_genes
d[name] = genelist
n[name] = genelist

count = 0
for proteome in proteomes:
    count = count +1
    percentage_list = list() #initiation of lists
    genename_list = list()
    
    for gene in genelist[1:]:
        print ("BLASTing %s on %s ..." %(gene, proteome))
        
        for qseq in SeqIO.parse(os.path.join(query_path, gene)+".fasta", "fasta"):
            print "Query: %i" %len(qseq.seq)
        
        blastp_cline = NcbiblastpCommandline(cmd = blastp_path, query = os.path.join(query_path, gene)+".fasta" , db = os.path.join(proteomes_path, proteome), evalue = 1 , outfmt = 5, out = data_path + "result.xml")
        stdout, stderr = blastp_cline()
        
        result_handle = open (data_path + "result.xml")

        blast_record = NCBIXML.read(result_handle) #check the results
        if len(blast_record.alignments) == 0:
                percentage = 0
                genename = "no hit"
        else:
        
            length = blast_record.alignments[0].length #just looking at the first hit (0) 
            title = blast_record.alignments[0].title #reading info from table
            if title.find("PE=") == -1:
                genename = title[title.find("GN=")+3:title.find("SV=")]
            if title.find("PE=") != -1:
                genename = title[title.find("GN=")+3:title.find("PE=")]                
            #genename_search = re.search("GN=;(.*)PE=", title)

            for hsp in blast_record.alignments[0].hsps:
                identity = hsp.identities
                
            print ("identities: %i" %hsp.identities)
            print "length: %i" %length
            print "alignlength: %i" %hsp.align_length
            percentage = (float(identity)/float(length)) *100
        
        print ("Identity percentage is %f %% \n" %percentage)
        
        percentage_list.append(percentage) #write best blast hit into the lists
        genename_list.append(genename)
        
    percentage_list.insert(0, proteome)
    genename_list.insert(0,proteome)
    

    d[proteome] = percentage_list #save results in a dict
    n[proteome] = genename_list




#saves both, the percentage values and gene names identical structured
with open(data_path + "final_results.csv", "wb") as csv_file: 
    writer = csv.writer(csv_file)
    writer.writerow(d[name])
    for proteome in proteomes:
        writer.writerow(d[proteome])
    #for key, value in d.items():
      #writer.writerow([key, value])

with open(data_path + "final_results_genenames.csv", "wb") as csv_file:
    writer = csv.writer(csv_file)
    writer.writerow(n[name])
    for proteome in proteomes:
        writer.writerow(n[proteome])

print "\ndone!"